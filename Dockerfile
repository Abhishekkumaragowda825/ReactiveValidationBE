# Use an official Python runtime as a parent image
FROM python:3.8

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Set the working directory in the container
WORKDIR /app

# Copy the Pipfile and Pipfile.lock to the container
COPY Pipfile Pipfile.lock /app/

# Install dependencies
RUN pip install pipenv && pipenv install --deploy --ignore-pipfile

# Install djangorestframework-jsonapi
RUN pipenv run pip install djangorestframework-jsonapi

# Copy the Django project files into the container
COPY . /app/

# Collect static files
RUN pipenv run python manage.py collectstatic --noinput

# Run database migrations
RUN pipenv run python manage.py migrate

# Expose the port on which the application will run
EXPOSE 8000

# Run the Django application
CMD ["pipenv", "run", "python", "manage.py", "runserver", "0.0.0.0:8000"]
