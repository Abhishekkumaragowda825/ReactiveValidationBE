import json
from django.shortcuts import render
from django.contrib.auth import authenticate

# Create your views here.
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.http import JsonResponse
from .models import Student
from rest_framework.response import Response
from django.contrib.auth.models import User

@api_view(['GET'])
def get_students(request): 
    data = Student.objects.values()
    return JsonResponse(list(data), safe=False)

@api_view(['POST'])
def create_student(request):
    raw_password = request.data['password']
    
    # Assuming you have a custom user model
    User.objects.create_user(
        username=request.data['username'],
        email=request.data['email'],
        password=raw_password,
    )
    instance = Student(name=request.data['name'],lname=request.data['lname'],
                    email=request.data['email'],gender=request.data['gender'],
                    message=request.data['message'], country=request.data['country'], 
                    username=request.data['username'], password=request.data['password'])
    instance.save()
    return Response("200 ok")

@api_view(['DELETE'])
def delete_student(request,pk):
    instance = Student.objects.get(id=pk)
    instance.delete()
    return Response("200 ok")

@api_view(['PUT'])
def update_student(request):
    instance = Student.objects.get(id=request.data['id'])
    instance.name=request.data['name']
    instance.lname=request.data['lname']
    instance.email=request.data['email']
    instance.gender=request.data['gender'],
    instance.message=request.data['message']
    instance.country=request.data['country']
    instance.save()
    return Response("200 ok")

@api_view(['POST'])
def authenticate_user(request):
    username = request.data['username']
    password = request.data['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        return Response("200 OK")
    else:
        return Response("Authentication failed", status=401)

